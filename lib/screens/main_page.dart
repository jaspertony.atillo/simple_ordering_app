import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_ordering_app/models/menu.dart';
import 'package:simple_ordering_app/providers/menu_provider.dart';
import 'package:simple_ordering_app/screens/cart_screen.dart';
import 'package:simple_ordering_app/screens/login_screen.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  SharedPreferences sharedPreferences;
  int count = 0;
  String stringValue;
  var isSelected = false;
  var mycolor = Colors.white;

  List<String> menuId = [];
  List<String> menuName = [];
  List<String> menuPrice = [];
  List<String> perItem = [];
  Map<int, bool> itemsSelectedValue = Map();

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
    MenuProvider().getAllMenu();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("userid") == null) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Menu List",
            textAlign: TextAlign.start,
            style: TextStyle(color: Colors.black, fontFamily: "Poppins")),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              sharedPreferences.clear();
              sharedPreferences.commit();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: Text("Log Out",
                style: TextStyle(color: Colors.black, fontFamily: "Poppins")),
          ),
        ],
      ),
      body: Container(
        child: FutureBuilder(
          future: MenuProvider().getAllMenu(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text('Loading'),
                ),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                bool isCurrentIndexSelected = itemsSelectedValue[index] == null
                    ? false
                    : itemsSelectedValue[index];
                return Center(
                  child: Card(
                    color: mycolor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    clipBehavior: Clip.antiAlias,
                    margin:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          itemsSelectedValue[index] = !isCurrentIndexSelected;
                          //toggleSelection(snapshot.data[index].menuid);
                          if (!menuId.contains(snapshot.data[index].menuid)) {
                            menuId.add(snapshot.data[index].menuid);
                            menuName.add(snapshot.data[index].menuname);
                            menuPrice.add(snapshot.data[index].menuprice);
                          }else{
                            menuId.remove(snapshot.data[index].menuid);
                            menuName.remove(snapshot.data[index].menuname);
                            menuPrice.remove(snapshot.data[index].menuprice);
                          }
                        });
                      },
                      onDoubleTap: () {
                        setState(() {
                          itemsSelectedValue[index] = !isCurrentIndexSelected;
                          //if(!menuId.contains(snapshot.data[index].menuid)){
                          menuId.remove(snapshot.data[index].menuid);
                          menuName.remove(snapshot.data[index].menuname);
                          menuPrice.remove(snapshot.data[index].menuprice);
                          //}
                        });
                      },
                      onLongPress: () {
                        setState(() {
                          itemsSelectedValue[index] = !isCurrentIndexSelected;
                          //if(!menuId.contains(snapshot.data[index].menuid)){
                          menuId.remove(snapshot.data[index].menuid);
                          menuName.remove(snapshot.data[index].menuname);
                          menuPrice.remove(snapshot.data[index].menuprice);
                          //}
                        });
                      },
                      child: Container(
                        color: isCurrentIndexSelected
                            ? Colors.grey[300]
                            : Colors.white,
                        height: 110.0,
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: 150.0,
                              margin: EdgeInsets.symmetric(horizontal: 0.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.network(
                                    'https://bigbys.e2econsultancy.ph/api/apitest/menu/' +
                                        snapshot.data[index].menuimg,
                                    height: 110.0,
                                    width: 140.0,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Flexible(
                                      child: RichText(
                                        overflow: TextOverflow.ellipsis,
                                        strutStyle: StrutStyle(fontSize: 13.0),
                                        text: TextSpan(
                                          style: TextStyle(color: Colors.black),
                                          text: snapshot.data[index].menuname,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Flexible(
                                      child: RichText(
                                        overflow: TextOverflow.ellipsis,
                                        strutStyle: StrutStyle(fontSize: 12.0),
                                        text: TextSpan(
                                          style: TextStyle(color: Colors.black),
                                          text: snapshot
                                              .data[index].menudescription,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          '₱ ' + snapshot.data[index].menuprice,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                          textAlign: TextAlign.start,
                                        ),
                                      ],
                                    ),
                                    // Expanded(
                                    //     child: isCurrentIndexSelected
                                    //         ? Align(
                                    //             alignment: FractionalOffset
                                    //                 .bottomRight,
                                    //             child: Padding(
                                    //               padding: EdgeInsets.only(
                                    //                   bottom: 1.0),
                                    //               child: Material(
                                    //                 borderRadius:
                                    //                     BorderRadius.only(
                                    //                   bottomRight:
                                    //                       Radius.circular(10.0),
                                    //                   topLeft:
                                    //                       Radius.circular(50.0),
                                    //                 ),
                                    //                 color: Colors.red,
                                    //                 child: InkWell(
                                    //                   splashColor: Colors.red,
                                    //                   child: SizedBox(
                                    //                     width: 40,
                                    //                     height: 40,
                                    //                     child: Center(
                                    //                       child: Text(
                                    //                         perItem.length
                                    //                             .toString(),
                                    //                         style: TextStyle(
                                    //                             color: Colors
                                    //                                 .white),
                                    //                       ),
                                    //                     ),
                                    //                   ),
                                    //                 ),
                                    //               ),
                                    //             ),
                                    //           )
                                    //         : Center()),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Stack(
            children: <Widget>[
              Icon(
                Icons.add_shopping_cart,
                color: Colors.white,
              ),
              menuId.length == 0
                  ? CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 9.0,
                      child: Text(
                        '0',
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  : CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 9.0,
                      child: Text(
                        menuId.length.toString(),
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
            ],
          ),
          backgroundColor: Colors.red,
          onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CartScreen(menuId: menuId, menuName: menuName, menuPrice: menuPrice,))),
              }),
    );
  }

  void toggleSelection(String index) {
    setState(() {
      if (isSelected) {
        mycolor = Colors.white;
        isSelected = false;
      } else {
        mycolor = Colors.grey[300];
        isSelected = true;
      }
    });
  }

  Align menuCount() {
    return Align(
      alignment: FractionalOffset.bottomRight,
      child: Padding(
        padding: EdgeInsets.only(bottom: 1.0),
        child: Material(
          // e// ye button (customised radius)
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(10.0),
            topLeft: Radius.circular(50.0),
          ),
          color: Colors.red,
          child: InkWell(
            splashColor: Colors.red, // inkwell onPress colour
            child: SizedBox(
              width: 40, height: 40, //customisable size of 'button'
              child: Center(
                child: Text(
                  count.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            onTap: () {}, // or use onPressed: () {}
          ),
        ),
      ),
    );
  }
}
