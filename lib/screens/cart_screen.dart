import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_ordering_app/screens/login_screen.dart';

class CartScreen extends StatefulWidget {
  final List<String> menuId;
  final List<String> menuName;
  final List<String> menuPrice;

  const CartScreen({Key key, this.menuId, this.menuName, this.menuPrice}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  SharedPreferences sharedPreferences;
  List<String> menuIds = [];
  List<String> menuNames = [];
  List<String> menuPrices =[];
  bool _isChecked = true;
  int count = 0;
  double sum = 0;
  String userid;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      menuIds = widget.menuId;
      menuNames =widget.menuName;
      menuPrices = widget.menuPrice;
      var newList= [menuIds, menuNames, menuPrices].expand((x) => x).toList();

      // const JsonEncoder encoder = JsonEncoder.withIndent('  ');
      String jsonTags = jsonEncode(menuIds);
      print(jsonTags);
      var resBody = {};
      for (var ids in menuIds){
        resBody["id"] = ids[0].toString();
        var user = {};
        user["data"] = resBody;
        String str = json.encode(user);
        print(str);
      }
      for (var names in menuNames){
        resBody["name"] = names;
      }
      for(var prices in menuPrices){
        resBody["price"] = prices;
      }
      var user = {};
      user["data"] = resBody;
      String str = json.encode(user);
      print(str);
      //
      // for (var i = 0; i < menuPrices.length; i++) {
      //   sum += menuPrices[i];
      // }
      sum = menuPrices.fold<double>(0, (prev, value) => prev + double.parse(value));
      print(sum);
      checkLoginStatus();

    });
  }
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      userid = sharedPreferences.getString("userid");
      print(userid);
    });
    placeOrderFunction();
  }
  bool rememberMe = false;

  void _onRememberMeChanged(bool newValue) => setState(() {
    rememberMe = newValue;

    if (rememberMe) {
      // TODO: Here goes your functionality that remembers the user.
    } else {
      // TODO: Forget the user

    }
  });

  void placeOrderFunction(){
    var now = new DateTime.now();
    String datenow = ('${now.year}-${now.month}-${now.day}');
    var resbody = {};
    resbody["orderdate"] = datenow;
    resbody["userid"] = userid;
    var summaryvalue = {};
    summaryvalue["summaryvalue"] = resbody;
    String str = json.encode(summaryvalue);
    print(str);

    var resbody2 = {};
    resbody2["menuid"] = menuIds;
    resbody2["quantityordered"] = "1";
    var detailvalue = {};
    detailvalue["detailvalue"] = resbody2;
    String str2 = json.encode(detailvalue);
    print(str2);

  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Your Basket',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Poppins',
            ),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          FlatButton(
            child: Icon(
              Icons.add_shopping_cart,
              color: Colors.red,
            ),
          ),
        ],
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            locationSection(),
            SizedBox(
              height: 10.0,
            ),
            orderSection(),
            SizedBox(
              height: 10.0,
            ),
            paymentSection(),
            SizedBox(
              height: 15.0,
            ),
            buttonSection(),
          ],
        ),
      ),
    );
  }
  Widget locationSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        clipBehavior: Clip.antiAlias,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.location_on,color: Colors.redAccent,),
                      ),
                      Expanded(
                        child: Text(
                         'Deliver to',
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 30.0,
                      ),
                      Expanded(
                        child: Text(
                          'San Marino Residence, Cebu City',
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
          ],
        )
      ),
    );
  }

  Widget orderSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          clipBehavior: Clip.antiAlias,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 15.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'Orders',
                            style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          width: 55.0,
                          height: 20.0,
                          child: RaisedButton(
                            color: Colors.grey,
                            onPressed: () {
                              setState(() {
                                menuNames.clear();
                                menuPrices.clear();
                                sum = 00.00;
                              });
                            },
                            elevation: 0.0,
                            child: Text("CLEAR", style: TextStyle(color: Colors.white70, fontSize: 6.0)),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                  height: 100.0,
                  child: getMenuNameListView()
              ),
              SizedBox(
                height: 100.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'Total Amount',
                            style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          '₱ $sum' + '0',
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40.0,
              ),
            ],
          )
      ),
    );
  }

  Widget paymentSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          clipBehavior: Clip.antiAlias,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 15.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'Payment',
                            style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.account_balance_wallet,color: Colors.black,),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'Cash',
                          ),
                        ),
                        Checkbox(
                          value: rememberMe,
                          onChanged: _onRememberMeChanged,
                        ),
                        // CheckboxListTile(
                        //   value: _isChecked,
                        //   onChanged: (val){},
                        //   title: Text(
                        //     ''
                        //   ),
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.keyboard,color: Colors.black,),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'Promo Code',
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'Enter Promo Code',
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.call_to_action,color: Colors.black,),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(
                            'PayMaya',
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'Connect Account',
                          ),
                        ),
                        // CheckboxListTile(
                        //   value: _isChecked,
                        //   onChanged: (val){},
                        //   title: Text(
                        //     ''
                        //   ),
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          )
      ),
    );
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 15.0),
      child: RaisedButton(
        color: Colors.red,
        onPressed: () {
          if(rememberMe == false){
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: new Text("Please select Cash/COD as payment option"),
                  actions: <Widget>[
                    FlatButton(
                      child: new Text("OK"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          }else{
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: new Text("YAWA"),
                  actions: <Widget>[
                    FlatButton(
                      child: new Text("OK"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          }
        },
        elevation: 0.0,
        child: Text("PLACE ORDER", style: TextStyle(color: Colors.white70, fontWeight: FontWeight.bold)),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
        ),
      ),
    );
  }

  ListView getMenuNameListView () {
    TextStyle titleStyle = Theme.of(context).textTheme.subhead;
    double width = MediaQuery.of(context).size.width;

    return ListView.builder(
      itemCount: menuNames.length,
      itemBuilder: (BuildContext context,int index){
        return Container(
          height: 30.0,
          child: ListTile(
            title: Text(this.menuNames[index], style: TextStyle(fontSize: 14.0,),),
            trailing: Text( '₱ ' + this.menuPrices[index], style: TextStyle(fontSize: 14.0,),),
            onLongPress: (){

            },
          ),
        );
      } ,
    );
  }

  Widget getListView(){
    var listview = ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.landscape),
          title: Text('Landscape'),
          subtitle: Text('Beautiful View'),
          trailing: Icon(Icons.wb_sunny),
        )
      ],
    );
    return listview;
  }
}


